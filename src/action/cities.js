import * as types from '../constants/ActionTypes'
import config from '../config'

export const addCityByCoors = coors => {
  return dispatch => {
    dispatch({ type: types.LOADING })
    fetch(`${config.apiUrl}/weather?appid=${config.apiKey}&lat=${coors.lat}&lon=${coors.lon}`)
      .then(response => response.json())
      .then(data => {
        if (data.cod && data.cod !== 200) {
          dispatch({ type: types.ERROR, message: data.message })
          return
        }
        dispatch({ type: types.LOADED })
        dispatch({ type: types.ADD_CITY, city: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const currentCity = () => {
  return dispatch => {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        addCityByCoors({ lat: position.coords.latitude, lon: position.coords.longitude })(dispatch)
      })
    } else {
      dispatch({ type: types.NOT_HAVE_GEOLOCATION })
    }
  }
}

export const addCityByName = cityName => {
  return dispatch => {
    dispatch({ type: types.LOADING })
    fetch(`${config.apiUrl}/weather?appid=${config.apiKey}&q=${cityName}`)
      .then(response => response.json())
      .then(data => {
        if (data.cod && data.cod !== 200) {
          dispatch({ type: types.ERROR, message: data.message })
          return
        }
        dispatch({ type: types.LOADED })
        dispatch({ type: types.ADD_CITY, city: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const updateCity = ids => {
  return dispatch => {
    dispatch({ type: types.UPDATING })
    fetch(`${config.apiUrl}/group?appid=${config.apiKey}&id=${ids.join(',')}`)
      .then(response => response.json())
      .then(data => {
        if (data.cod && data.cod !== 200) {
          dispatch({ type: types.ERROR, message: data.message })
          return
        }
        dispatch({ type: types.UPDATED })
        dispatch({ type: types.UPDATE_CITIES, cities: data.list })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const removeCity = id => ({ type: types.REMOVE_CITY, id })
