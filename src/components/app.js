import React, { useEffect } from 'react'
import Cities from '../containers/cities'

const App = ({ actions }) => {
  useEffect(() => {
    actions.currentCity()
  }, [])

  return <Cities />
}

export default App
