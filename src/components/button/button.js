import React from 'react'
import './button.css'

const Button = props => (
  <button type="button" className={['btn', props.className].join(' ')} onClick={props.onClick}>
    {props.children}
  </button>
)

export default Button
