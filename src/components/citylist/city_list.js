import React, { useEffect, useState } from 'react'
import Button from '../button/button'
import Input from '../input/input'
import Card from '../card/card'
import Error from '../error/error'

import './city_list.css'

const CityList = ({ cities, ui, actions }) => {
  const [cityName, setCityName] = useState(null)

  let intervalId = null
  useEffect(() => {
    intervalId = setInterval(() => {
      if (cities.length > 0) {
        actions.updateCity(cities.map(c => c.id))
      }
    }, 5000)

    return () => {
      clearInterval(intervalId)
    }
  }, [cities])

  const handleSubmit = event => {
    if (cities.length < 20) {
      actions.addCityByName(cityName)
    }

    event.preventDefault()
    event.stopPropagation()
  }

  const handleChangeText = event => {
    setCityName(event.target.value)
  }

  const handleClose = id => () => {
    actions.removeCity(id)
  }

  return (
    <div className="citylist">
      <div className="citylist__search">
        <form onSubmit={handleSubmit}>
          <div className="citylist__search_form_input">
            <Input type="text" placeholder="Name of city" className="search_form_input" onChange={handleChangeText} />
            <Button disabled={ui.isLoading} className="btn_blue" onClick={handleSubmit}>
              Find City
            </Button>
          </div>
        </form>
      </div>
      {ui.isError && <Error onClose={() => actions.clearError()}>{ui.errorMessage}</Error>}
      <div className="citylist__cards">
        {(cities || []).map(city => (
          <Card key={city.id} headerText={city.name} onClose={handleClose(city.id)}>
            <ul>
              <li>Temp: {(parseFloat(city.main.temp) - 273).toFixed(2)}</li>
              <li>
                Weather: {city.weather[0].main}, ({city.weather[0].description})
              </li>
              <li>Wind: {city.wind.speed} m/s</li>
            </ul>
          </Card>
        ))}
      </div>
    </div>
  )
}

export default CityList
