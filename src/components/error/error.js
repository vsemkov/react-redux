import React from 'react'
import Button from '../button/button'
import CloseIcon from '../closeicon/close-icon'

import './error.css'

const Error = ({ onClose, children }) => {
  return (
    <div className="error">
      <div className="error__text">{children}</div>
      <Button onClick={onClose} className="error_close_button">
        <CloseIcon />
      </Button>
    </div>
  )
}

export default Error
