import React from 'react'
import './input.css'

const Input = props => <input {...props} className={['input', props.className].join(' ')} />

export default Input
