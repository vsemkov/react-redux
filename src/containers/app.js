import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import App from '../components/app'
import * as CityActions from '../action/cities'

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(CityActions, dispatch)
})

export default connect(
  null,
  mapDispatchToProps
)(App)
