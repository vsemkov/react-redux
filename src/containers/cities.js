import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as CityActions from '../action/cities'
import * as UIActions from '../action/ui'
import CitiesList from '../components/citylist/city_list'

const mapStateToProps = state => {
  return {
    cities: state.cities.list,
    ui: state.ui
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ...CityActions,
      ...UIActions
    },
    dispatch
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CitiesList)
