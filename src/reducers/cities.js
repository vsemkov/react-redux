import { ADD_CITY, REMOVE_CITY, UPDATE_CITIES } from '../constants/ActionTypes'

const loadData = () => {
  const json = localStorage.getItem('CITIES')
  if (json === null) {
    return []
  }

  return JSON.parse(json)
}

const saveData = data => {
  const json = JSON.stringify(data)
  localStorage.setItem('CITIES', json)
}

const initialState = {
  list: loadData()
}

export default function cities(state = initialState, action) {
  switch (action.type) {
    case REMOVE_CITY: {
      const list = state.list.filter(item => item.id !== action.id)

      saveData(list)

      return {
        ...state,
        list
      }
    }
    case ADD_CITY: {
      const isExist = (state.list || []).filter(item => item.id === action.city.id).length > 0
      if (isExist) {
        return state
      }

      const list = state.list.concat(action.city)

      saveData(list)

      return {
        ...state,
        list
      }
    }
    case UPDATE_CITIES: {
      saveData(action.cities)

      return {
        ...state,
        list: action.cities
      }
    }
    default:
      return state
  }
}
