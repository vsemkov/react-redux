import cities from './cities'
import ui from './ui'

export default {
  cities,
  ui
}
